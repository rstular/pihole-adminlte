<?php /*
*	 Pi-hole: A black hole for Internet advertisements
*	 (c) 2017 Pi-hole, LLC (https://pi-hole.net)
*	 Network-wide ad blocking via your own hardware.
*
*	 This file is copyright under the latest version of the EUPL.
*	 Please see LICENSE file for your rights under this license. */
	require "scripts/pi-hole/php/header.php";
	
	$setupVars = parse_ini_file("/etc/pihole/setupVars.conf");
	
	function bytes_to_human($bytes) {
		$si_prefix = array('B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB');
		$decimals = 2;
		$class = min((int)log($bytes , 1024) , count($si_prefix) - 1);
		$bytes = sprintf('%1.2f' , $bytes / pow(1024,$class)) . ' ' . $si_prefix[$class];
		return $bytes;
	}
	
	// --------------------------
	
	// Try to get temperature value from different places (OS dependent)
	if(file_exists("/sys/class/thermal/thermal_zone0/temp"))
	{
		$output = rtrim(file_get_contents("/sys/class/thermal/thermal_zone0/temp"));
	}
	elseif (file_exists("/sys/class/hwmon/hwmon0/temp1_input"))
	{
		$output = rtrim(file_get_contents("/sys/class/hwmon/hwmon0/temp1_input"));
	}
	else
	{
		$output = "";
	}

	// Test if we succeeded in getting the temperature
	if(is_numeric($output))
	{
		// $output could be either 4-5 digits or 2-3, and we only divide by 1000 if it's 4-5
		// ex. 39007 vs 39
		$celsius = intVal($output);

		// If celsius is greater than 1 degree and is in the 4-5 digit format
		if($celsius > 1000) {
			// Use multiplication to get around the division-by-zero error
			$celsius *= 1e-3;
		}

		$kelvin = $celsius + 273.15;
		$fahrenheit = ($celsius*9./5)+32.0;

		if(isset($setupVars['TEMPERATUREUNIT']))
		{
			$temperatureunit = $setupVars['TEMPERATUREUNIT'];
		}
		else
		{
			$temperatureunit = "C";
		}
		// Override temperature unit setting if it is changed via Settings page
		if(isset($_POST["tempunit"]))
		{
			$temperatureunit = $_POST["tempunit"];
		}
	}
	else
	{
		// Nothing can be colder than -273.15 degree Celsius (= 0 Kelvin)
		// This is the minimum temperature possible (AKA absolute zero)
		$celsius = -273.16;
	}

	// --------------------------------
	
	// Get memory usage
	$data = explode("\n", file_get_contents("/proc/meminfo"));
	$meminfo = array();
	if(count($data) > 0)
	{
		foreach ($data as $line) {
			$expl = explode(":", trim($line));
			if(count($expl) == 2) {
				// remove " kB" from the end of the string and make it an integer
				$meminfo[$expl[0]] = intVal(substr($expl[1],0, -3));
			}
		}
		$memory_used = $meminfo["MemTotal"]-$meminfo["MemFree"]-$meminfo["Buffers"]-$meminfo["Cached"];
		$memory_total = $meminfo["MemTotal"];
		$memory_usage = $memory_used/$memory_total;
	} else {
		$memory_usage = -1;
	}
	
	// --------------------------
	
	// Get load
	$loaddata = sys_getloadavg();
	foreach ($loaddata as $key => $value) {
		$loaddata[$key] = round($value, 2);
	}
	// Get number of processing units available to PHP
	// (may be less than the number of online processors)
	$nproc = shell_exec('nproc');
	if(!is_numeric($nproc))
	{
		$cpuinfo = file_get_contents('/proc/cpuinfo');
		preg_match_all('/^processor/m', $cpuinfo, $matches);
		$nproc = count($matches[0]);
	}
	
	// ---------------------------
	
	// Get log file size
	$log_size = filesize("/var/log/pihole.log");
	$log_size = bytes_to_human($log_size);
	
	// Get FTL log size
	$ftl_log_size = filesize("/var/log/pihole-FTL.log");
	$ftl_log_size = bytes_to_human($ftl_log_size);
	
	// Get free disk space
	$df = disk_free_space("/");
	$dt = disk_total_space("/");
	$du = $dt - $df;
	$dp = sprintf('%.2f',($du / $dt) * 100);
	
	// ---------------------------
	
	$rx_bytes = 0;
	$tx_bytes = 0;
	$rbps = 0;
	$tbps = 0;
	
	if (isset($setupVars["PIHOLE_INTERFACE"])) {
		$piHoleInterface = $setupVars["PIHOLE_INTERFACE"];
		$tx[] = @file_get_contents("/sys/class/net/$piHoleInterface/statistics/tx_bytes");
		$rx[] = @file_get_contents("/sys/class/net/$piHoleInterface/statistics/rx_bytes");
		sleep(1);
		$tx[] = @file_get_contents("/sys/class/net/$piHoleInterface/statistics/tx_bytes");
		$rx[] = @file_get_contents("/sys/class/net/$piHoleInterface/statistics/rx_bytes");
		
		$rx_bytes = bytes_to_human($rx[1]);
		$tx_bytes = bytes_to_human($tx[1]);
		
		$tbps = bytes_to_human($tx[1] - $tx[0]) . "ps";
		$rbps = bytes_to_human($rx[1] - $rx[0]) . "ps";
	} else {
		$piHoleInterface = "unknown";
	}
?>

<div class="row">
	<div class="col-md-12">
	<h1>Status</h1>
	
	<!-- Performance information - begin -->
	
	<h3>Performance</h3>
	<?php
	echo "<a id=\"temperature\"><i class=\"fa fa-fire\" style=\"color:";
	if ($celsius > 60 || $celsius === -273.15) {
		echo "#FF0000";
	} else {
		echo "#3366FF";
	}
	echo "\"></i> Temperature:&nbsp;";
	if ($celsius !== -273.15) {
		if($temperatureunit === "F") {
			echo round($fahrenheit,1) . "&nbsp;&deg;F";
		} elseif($temperatureunit === "K") {
			echo round($kelvin,1) . "&nbsp;K";
		} else {
			echo round($celsius,1) . "&nbsp;&deg;C";
		}
	} else {
		echo "N/A";
	}
	echo "</a><br>";
	
	echo "<a><i class=\"fa fa-memory\" style=\"color:";
	if ($memory_usage > 0.75 || $memory_usage < 0.0) {
		echo "#FF0000";
	} else {
		echo "#3366FF";
	}
	if ($memory_usage > 0.0) {
		echo "\"></i> Memory usage:&nbsp;&nbsp;" . sprintf("%.1f",100.0*$memory_usage) . "&thinsp;%</a><br>";
	} else {
		echo "\"></i> Memory usage:&nbsp;&nbsp; N/A</a><br>";
	}
	
	echo "<a><i class=\"fa fa-microchip\" style=\"color:3366FF\"></i> Detected processing units: $nproc</a><br>";
	
	echo "<a><i class=\"fa fa-cogs\" style=\"color:";
	if ($loaddata[0] > $nproc) {
		echo "#FF0000";
	} else {
		echo "#3366FF";
	}
	echo "\"></i> Load:&nbsp;&nbsp;" . $loaddata[0]*100 . "% (last minute)&nbsp;&nbsp;" . $loaddata[1]*100 . "% (last 5 minutes)&nbsp;&nbsp;". $loaddata[2]*100 . "% (last 15 minutes)</a><br>";
	?>
	
	<!-- Performance information - end -->
	
	<!-- Disk and storage information - begin -->

	<h3>Disk and storage</h3>
	<?php
	// Log file size
	echo "<a><i class=\"fa fa-file\" style=\"color:#3366FF\"></i> Log file size: $log_size</a><br>";
	
	// FTL log file size
	echo "<a><i class=\"fa fa-file\" style=\"color:#3366FF\"></i> FTL log file size: $ftl_log_size</a><br>";
	
	// Disk usage [in %]
	echo "<a><i class=\"fa fa-hdd\" style=\"color:";
	if ($dp > 80) {
		echo "#FF0000";
	} else {
		echo "#3366FF";
	}
	echo "\"></i> Disk usage: $dp%</a><br>";
	
	$df = bytes_to_human($df);
	$dt = bytes_to_human($dt);
	$du = bytes_to_human($du);
	
	// Free, used and total hard drive space
	echo "<a><i class=\"fa fa-hdd\" style=\"color:#3366FF\"></i> Free disk space: $df</a><br>";
	
	echo "<a><i class=\"fa fa-hdd\" style=\"color:#3366FF\"></i> Used disk space: $du</a><br>";
	
	echo "<a><i class=\"fa fa-hdd\" style=\"color:#3366FF\"></i> Total disk space: $dt</a><br>";
	?>
	
	<!-- Disk and storage information - end -->
	
	<!-- Network information - begin -->
	
	<h3>Network information</h3>
	<?php
	echo "<a><i class=\"fa fa-sitemap\" style=\"color:#3366FF\"></i> Network interface: $piHoleInterface</a><br>";
	
	if (isset($setupVars["IPV4_ADDRESS"])) {
		$piHoleIPv4 = $setupVars["IPV4_ADDRESS"];
	} else {
		$piHoleIPv4 = "unknown";
	}
	echo "<a><i class=\"fa fa-terminal\" style=\"color:#3366FF\"></i> IPv4 address: $piHoleIPv4</a><br>";
	
	echo "<a><i class=\"fa fa-download\" style=\"color:#3366FF\"></i> Total recieved data: $rx_bytes</a><br>";
	
	echo "<a><i class=\"fa fa-download\" style=\"color:#3366FF\"></i> Current inbound traffic rate: $rbps</a><br>";
	
	echo "<a><i class=\"fa fa-upload\" style=\"color:#3366FF\"></i> Total sent data: $tx_bytes</a><br>";
	
	echo "<a><i class=\"fa fa-upload\" style=\"color:#3366FF\"></i> Current outbound traffic rate: $tbps</a><br>";
	?>
	</div>
</div>

<?php
	require "scripts/pi-hole/php/footer.php";
?>
